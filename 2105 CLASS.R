salesData <- read.csv( "Sales-Win-Loss.csv" )
salesData

#Data's summary
str(salesData)

#spliting to train set and test set
split <-runif(nrow(salesData))>0.3

salesTrain <- salesData[split,]
salesTest <- salesData[!split,]

#senity check - to check that the sum of nrows is the num of raws in raw data.
nrow(salesTrain)
nrow(salesTest)

#nedde package for this EX
install.packages('rpart')
library(rpart)

install.packages('rpart.plot')
library(rpart.plot)


#comute the decision tree model
dcModel <-rpart(Opportunity.Result ~ Supplies.Subgroup + Supplies.Group + Region + Route.To.Market + Elapsed.Days.In.Sales.Stage 
                + Sales.Stage.Change.Count + Total.Days.Identified.Through.Closing + Total.Days.Identified.Through.Qualified
                + Opportunity.Amount.USD + Client.Size.By.Revenue + Client.Size.By.Employee.Count + Revenue.From.Client.Past.Two.Years 
                + Competitor.Type + Ratio.Days.Identified.To.Total.Days + Ratio.Days.Validated.To.Total.Days +  Ratio.Days.Qualified.To.Total.Days
                + Deal.Size.Category, data=salesTrain, method="class")

rpart.plot::rpart.plot(dcModel, fallen.leaves=FALSE)

#preparingfor coputing the confusion matrix
predicted  <-predict(dcModel, salesTest[,-7], type ="class")

conv_10 <-function(x){
  x <-ifelse(x =='Won', 1,0)
}

predicted01 <-sapply(predicted,conv_10)
actual01 <-sapply(salesTest$Opportunity.Result, conv_10)

#comuting confusion matrix
install.packages('SDMTolls')
library(SDMTools)

confusion.matrix(actual01,predicted01)


#preparingfor coputing the confusion matrix - trainig set
predicted1  <-predict(dcModel, salesTrain[,-7], type ="class")

conver_10 <-function(x){
  x <-ifelse(x =='Won', 1,0)
}

predicted01_1 <-sapply(predicted1,conver_10)
actual01_1 <-sapply(salesTrain$Opportunity.Result, conver_10)


confusion.matrix(actual01_1,predicted01_1)

