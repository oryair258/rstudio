vectorWithMissing <- c(1,3,4,NA,7,NA)

#check where there are NA's
is.na(vectorWithMissing)

#counting NA's
sum(is.na(vectorWithMissing))

#rate of missing values(NA's)
sum(is.na(vectorWithMissing))/length(vectorWithMissing)

#function to calculate rate of missing values in vector
pMiss <-function(x){sum(is.na(x))/length(x)*100}
pMiss (vectorWithMissing)

#Air quality data set (existing data set)
data <- airquality
data

#has data on NA's also
summary(data)

#apply - using specific function on all data (1-rows, 2-columns)
apply(data,2,pMiss)

#Mice R packahe with functions for dealing with missing values
install.packages('mice')
library('mice')

#analyzing missing data
md.pattern(data)


#Data imputation

#------------------1----------------------------
#Imputation By mean
tempDataMean <- mice (data,m=1,maxit=1,meth='mean',seed=500)

#Imputation suggestion
tempDataMean$imp$Ozone
tempDataMean$imp$Solar.R

#inpute data seggestion
completeDataMean <-complete(tempDataMean,1)
completeDataMean

#------------------2----------------------------
#Imputation By pmm - m seggestions to NA's by linear regression
#(data, num of options, max iteration, method, Z0)
tempDataPMM <- mice (data,m=5,maxit=50,meth='pmm',seed=500)

#Imputation suggestion
tempDataPMM$imp$Ozone
tempDataPMM$imp$Solar.R

#inpute data seggestion (data, num of option)
completeDataPMM <-complete(tempDataPMM,1)
completeDataPMM

#outlier detection and correction
outlier_values <- boxplot.stats(completeDataPMM$Ozone, coef = 1.5)$out
box_values <- boxplot.stats(completeDataPMM$Ozone, coef = 1.5)$stats


cap <- function(x){if(x<box_values[1]) x<-box_values[1]
else if (x>box_values[5]) x<-box_values[5]
return(x)}


sapply(completeDataPMM$Ozone,cap)


#normalize values
normalize <- function(x) {(x-min(x))/(max(x)-min(x))}

normalize(completeDataPMM$Ozone)
norm <-apply (completeDataPMM,2, normalize)
norm

completeDataPMM$Ozone
